# Data

## Dictionary

There is one dictionary:

```py
data = {
    "north_america": {
        "area": 24256000,
        "countries": ["Canada", "USA", "..."]
    },
    "africa": {
        "area": 30065000,
        "countries": ["Tunisia", "Morocco", "Algeria", "..."]
    },
    "europe": {
        "area": 9938000,
        "countries": ["France", "Belgium", "..."]
    }
}
```

This is a dictionary of... dictionaries!

There are three sub-dictionaries:

* `north_america`:

```py
north_america = {
    "area": 24256000,
    "countries": ["Canada", "USA", "..."]
}
```

* `africa`:

```py
africa = {
    "area": 30065000,
    "countries": ["Tunisia", "Morocco", "Algeria", "..."]
}
```

* `europe`:

```py
europe = {
    "area": 9938000,
    "countries": ["France", "Belgium", "..."]
}
```

Each sub-dictionary is... an other dictionary!

There are two *keys*:

* `area` which has as *value* one **integer**;
* `countries` which has as *value* a **list (of strings)**.


How to get (one line):

* the area of the Europe?
* the first country in North America?
* the count of countries in Africa?


How to calculate (several lines):

* the sum of the area?
* the list of all the countries?

Use [list comprehension](https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions) to do the same in one line! (optional)


## List and dictionary

Here a list of list with the name of a student and his marks in Mathematics, English and History :

```py
data_list = [
    ["Georges Washington", 10, 7, 6],
    ["John Adams", 8, 9, 10],
    ["Thomas Jefferson", 6, 9, 9]
]
```

### Get data

How to get:

* the English mark of Georges Washington?
* the History mark of John Adams?
* the name of the first student?

### Go throw data

Print the data as:

```
Marks of Georges Washington: 10/10 in Mathematics, 7/10 in English and 6/10 in History.
Marks of John Adams: 8/10 in Mathematics, 9/10 in English and 10/10 in History.
Marks of Thomas Jefferson: 6/10 in Mathematics, 9/10 in English and 9/10 in History.
```

### Transform data

Transform this list of list to have a dictionary of dictionaries:

* first dictionary:
    * keys: name of the student;
    * values: the second dictionaries;
* second dictionary:
    * keys: name of the lesson;
    * values: mark in the lesson.

The dictionary should be:

```py
data_dict = {
    "Georges Washington": {
        "mathematics": 10,
        "english": 7,
        "history": 6
    },
    "John Adams": {
        "mathematics": 8,
        "english": 9,
        "history": 10
    },
    "Thomas Jefferson": {
        "mathematics": 6,
        "english": 9,
        "history": 9
    }
}
```

### Go throw data

Print the data from the dictionary.


## Correction

### Dictionary

#### How to get (one line)

* the area of the Europe?

```py
area_europe = data["europe"]["area"]
print(area_europe)
```

* the first country in North America?

```py
north_america_first_country = data["north_america"]["countries"][0]
print(north_america_first_country)
```

* the count of countries in Africa?

```py
number_countries_africa = len(data["africa"]["countries"])
print(number_countries_africa)
```


#### How to calculate (several lines)

* the sum of the area?

```py
area_sum = 0
for continent_name, continent in data.items():
    area_sum += continent["area"]
print(area_sum)
```

* the list of all the countries?

```py
countries = []
for continent_name, continent in data.items():
    for country in continent["countries"]:
        countries.append(country)
print(countries)
```

#### Use [list comprehension](https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions) to do the same in one line!

* the sum of the area?

```py
area_sum = sum([continent["area"] for continent in data.values()])
print(area_sum)
```

* the list of all the countries?

```py
countries = [country for continent in data.values() for country in continent["countries"]]
print(countries)
```


### List and dictionary

#### Get data

```py
# the English mark of Georges Washington?
print(data_list[0][1])
# the History mark of John Adams?
print(data_list[1][3])
# the name of the first student?
print(data_list[0][0])
```

#### Go throw data

Print:

```
Marks of Georges Washington: 10/10 in Mathematics, 7/10 in English and 6/10 in History.
Marks of John Adams: 8/10 in Mathematics, 9/10 in English and 10/10 in History.
Marks of Thomas Jefferson: 6/10 in Mathematics, 9/10 in English and 9/10 in History.
```

```py
pattern = "Marks of {name}: {mathematics}/10 in Mathematics, {english}/10 in English and {history}/10 in History."
for student in data_list:
    print(
        pattern.format(
            name=student[0],
            mathematics=student[1],
            english=student[2],
            history=student[3]
        )
    )
```

### Transform data

Make a dictionary from the list...

```py
data_dict = {}
for student in data_list:
    data_dict[student[0]] = {
        "mathematics": student[1],
        "english": student[2],
        "history": student[3]
    }
print(data_dict)
```

#### Go throw data

Now it is more convenient to use data...

With a pattern 1:

```py
pattern = "Marks of {name}: {mathematics}/10 in Mathematics, {english}/10 in English and {history}/10 in History."
for name, mark in data_dict.items():
    print(
        pattern.format(
            name=name,
            mathematics=mark["mathematics"],
            english=mark["english"],
            history=mark["history"]
        )
    )
```

With a pattern 2:

```py
pattern = "Marks of {name}: {mathematics}/10 in Mathematics, {english}/10 in English and {history}/10 in History."
for name, mark in data_dict.items():
    print(pattern.format(name=name, **mark))
```

With a f-string:

```py
for name, mark in data_dict.items():
    print(f"Marks of {name}: {mark["mathematics"]}/10 in Mathematics, {mark["english"]}/10 in English and {mark["history"]}/10 in History.")
```
