# -*- coding: utf-8 -*-

"""main.py
    This file runs all the process of the project, it:
        - loads of the XML data;
        - transforms XML data in geom;
        - writes Shapefile.
"""

# Import dependencies
from geo import write_shapefile, xml_to_geom
from xml_file import read_xml

# Process

# 1) read data
xml = read_xml('input_path.xml')

# 2) transform the data in geometry
geom = xml_to_geom(xml)

# 3) write data and get result
success = write_shapefile(geom, 'output_path.shp')


# Output
if success:
    print("Data has been processed")
else:
    print("Error !")
